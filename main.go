package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"spambuster/internal/config"
	"spambuster/internal/log"
	"spambuster/internal/spam"
	"time"
)

func main() {
	// flags & env vars
	confPath := flag.String("config", "config.toml", "Path to the config file")
	ai := flag.Bool("ai", false, "Use some very clever AI to rate potential spam messages ;)")
	created := flag.String("created", time.Time{}.Format(time.RFC3339), "Filter on creation date in ISO 8601 format (2019-03-15T08:00:00Z)")
	progress := flag.Bool("progress", false, "Print progress to stdout")
	issue := flag.Bool("issue", false, "Print output as Gitlab Issue-body (JSON)")

	token := os.Getenv("GITLAB_API_TOKEN")
	aiKey := os.Getenv("OPENAI_API_KEY")

	flag.Parse()

	log.Enable(*progress)
	log.Print("--- spambuster ---")

	// validate env vars
	if token == "" {
		fmt.Println("Gitlab token not set. Please specify with environment variable GITLAB_API_TOKEN.")
		os.Exit(2)
	}
	if *ai && aiKey == "" {
		fmt.Println("OpenAI key not set. Please specify with environment variable OPENAI_API_KEY.")
		os.Exit(3)
	}
	if !*ai {
		aiKey = ""
	}

	// load configuration
	s, err := config.LoadSettings(*confPath)
	if err != nil {
		panic(err)
	}

	// search for spammers
	buster := spam.NewBuster(*s, token, aiKey)
	spammers, err := buster.FindSpammers(*created)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// output
	if *issue {
		printIssueJson(spammers, *created)
	} else {
		for _, spammer := range spammers {
			fmt.Println(spammer.Name, "("+spammer.Username+")", "- Rating:", spammer.SpamRating, "- Messages:", len(spammer.Messages), "- UID:", spammer.ID)
		}
	}
}

func printIssueJson(spammers []spam.Spammer, created string) {
	createdTime, _ := time.Parse(time.RFC3339, created)

	text := fmt.Sprintf("Analyzed new Issues/Snippets created after: **%s**  \n", createdTime.Format(time.RFC1123))
	text += "Spam rating: 0 to 10 (low to high probability)\n\n---\n"
	for _, spammer := range spammers {
		text += fmt.Sprintf("User: **[%s](%s)** (%s)  \n", spammer.Username, spammer.URL, spammer.Name)
		text += fmt.Sprintf("Rating: **%s**  \n", spammer.SpamRating)
		text += fmt.Sprintf("Messages: **%d**  \n\n---\n", len(spammer.Messages))
	}
	if len(spammers) == 0 {
		text += "Nothing found :shrug:"
	}
	output := struct {
		Title       string `json:"title"`
		Description string `json:"description"`
	}{
		"Spam analysis - " + time.Now().UTC().Format("2006-01-02"),
		text,
	}
	b, err := json.Marshal(output)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println(string(b))
}
