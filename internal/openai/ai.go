package openai

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"time"
)

const (
	instructions = "You are a spam detection bot. Analyze if the text is a spam-message. Your answer must be a number (integer) from 0 (no spam) to 10 (definitely spam)."
	url          = "https://api.openai.com/v1/chat/completions"
)

type Question struct {
	Model       string    `json:"model"`
	Messages    []Message `json:"messages"`
	Temperature float64   `json:"temperature"`
}

type Message struct {
	Role    string `json:"role"`
	Content string `json:"content"`
}

type Answer struct {
	ID      string `json:"id"`
	Object  string `json:"object"`
	Created int    `json:"created"`
	Model   string `json:"model"`
	Usage   struct {
		PromptTokens     int `json:"prompt_tokens"`
		CompletionTokens int `json:"completion_tokens"`
		TotalTokens      int `json:"total_tokens"`
	} `json:"usage"`
	Choices []struct {
		Message      Message `json:"message"`
		FinishReason string  `json:"finish_reason"`
		Index        int     `json:"index"`
	} `json:"choices"`
}

func GetAiRating(text string, apiKey string) (*Answer, error) {
	client := http.Client{Timeout: 15 * time.Second}

	if len(text) > 4096 {
		text = text[0:4096]
	}

	question := Question{
		Model: "gpt-3.5-turbo",
		Messages: []Message{
			{
				Role:    "system",
				Content: instructions,
			},
			{
				Role:    "user",
				Content: text,
			},
		},
		Temperature: 0,
	}

	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(question)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", url, &buf)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", "Bearer "+apiKey)
	req.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	answer := &Answer{}
	err = json.Unmarshal(b, answer)
	if err != nil {
		return nil, err
	}

	return answer, nil
}
