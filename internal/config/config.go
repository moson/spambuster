package config

import "github.com/BurntSushi/toml"

type Settings struct {
	GitlabUrl           string   `toml:"gitlab_url"`
	GitlabApiPerPage    int      `toml:"gitlab_api_per_page"`
	SpamKeywords        []string `toml:"spam_keywords"`
	SafeUserIdThreshold int      `toml:"safe_userid_threshold"`
	SafeUserIds         []int    `toml:"safe_userids"`
	RatingNum           float64  `toml:"rating_num"`
}

func LoadSettings(path string) (*Settings, error) {
	s := &Settings{}

	_, err := toml.DecodeFile(path, s)
	if err != nil {
		return nil, err
	}

	return s, nil
}
