package log

import "fmt"

var enabled = false

func Print(message ...any) {
	if enabled {
		fmt.Println(message...)
	}
}

func Enable(enable bool) {
	enabled = enable
}
