package gitlab

import (
	"encoding/json"
	"fmt"
	"net/http"
	netUrl "net/url"
	"spambuster/internal/log"
	"strconv"
	"strings"
	"time"
)

const (
	issuesUrl   = "/api/v4/issues?scope=all&confidential=false&per_page=%d&created_after=%s"
	snippetsUrl = "/api/v4/snippets/public?per_page=%d&created_after=%s"
)

var searchUrls = []string{issuesUrl, snippetsUrl}

type SearchResult struct {
	URL       string    `json:"web_url"`
	Title     string    `json:"title"`
	Text      string    `json:"description"`
	CreatedAt time.Time `json:"created_at"`
	Author    struct {
		ID       int    `json:"id"`
		Username string `json:"username"`
		Name     string `json:"name"`
		State    string `json:"state"`
		URL      string `json:"web_url"`
	} `json:"author"`
}

func Search(gitlabUrl, token string, perPage int, created string) ([]SearchResult, error) {
	results := []SearchResult{}

	_, err := time.Parse(time.RFC3339, created)
	if err != nil {
		return nil, err
	}

	for _, relUrl := range searchUrls {
		url := strings.TrimRight(gitlabUrl, "/") + fmt.Sprintf(relUrl, perPage, created)

		urlParsed, err := netUrl.Parse(url)
		if err != nil {
			return nil, err
		}

		log.Print("Path/Resource:", urlParsed.Path)

		result, err := searchApi(url, token)
		if err != nil {
			return nil, err
		}
		results = append(results, result...)
	}

	return results, nil
}

func searchApi(url, token string) ([]SearchResult, error) {
	client := http.Client{Timeout: 5 * time.Second}
	results := []SearchResult{}

	totalPages := 1
	currentPage := 1

	for {
		log.Print("Page", currentPage)
		reqUrl := url + fmt.Sprintf("&page=%d", currentPage)

		req, err := http.NewRequest("GET", reqUrl, nil)
		if err != nil {
			return nil, err
		}
		req.Header.Add("Authorization", "Bearer "+token)

		resp, err := client.Do(req)
		if err != nil {
			return nil, err
		}

		res := []SearchResult{}
		decoder := json.NewDecoder(resp.Body)
		err = decoder.Decode(&res)
		if err != nil {
			return nil, err
		}

		resp.Body.Close()
		results = append(results, res...)

		totalPages, err = strconv.Atoi(resp.Header.Get("x-total-pages"))
		if err != nil {
			return nil, err
		}

		if currentPage == totalPages {
			break
		}
		currentPage++
	}
	return results, nil
}
