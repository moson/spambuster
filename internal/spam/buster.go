package spam

import (
	"math"
	"sort"
	"spambuster/internal/config"
	"spambuster/internal/gitlab"
	"spambuster/internal/log"
	"spambuster/internal/openai"
	"strconv"
	"strings"
	"time"

	"golang.org/x/exp/slices"
)

type Spammer struct {
	ID          int
	Username    string
	Name        string
	URL         string
	Messages    []Message
	LastMessage time.Time
	SpamRating  string
}

type Message struct {
	URL       string
	Title     string
	Text      string
	CreatedAt time.Time
}

type Buster struct {
	settings    config.Settings
	gitlabToken string
	openAiKey   string
	print       func(a ...any)
}

func NewBuster(s config.Settings, gitlabToken string, openAiKey string) Buster {
	b := Buster{
		settings:    s,
		gitlabToken: gitlabToken,
		openAiKey:   openAiKey,
	}
	return b
}

func (b *Buster) FindSpammers(created string) ([]Spammer, error) {
	log.Print("Searching Gitlab for spam...")
	defer func() {
		log.Print()
		log.Print("Completed searching and rating process:")
		log.Print()
	}()

	results, err := gitlab.Search(b.settings.GitlabUrl, b.gitlabToken, b.settings.GitlabApiPerPage, created)
	if err != nil {
		return nil, err
	}

	spammers := map[string]Spammer{}
	result := []Spammer{}

	for _, entry := range results {
		// skip safe user id's (threshold)
		if entry.Author.ID < b.settings.SafeUserIdThreshold {
			continue
		}
		// skip safe user id's (list of ID's)
		if slices.Contains(b.settings.SafeUserIds, entry.Author.ID) {
			continue
		}

		// restructure search results into a list of spammers
		if spammer, exists := spammers[entry.Author.Username]; exists {
			spammer.Messages = append(spammer.Messages, Message{
				URL:       entry.URL,
				Title:     entry.Title,
				Text:      entry.Text,
				CreatedAt: entry.CreatedAt,
			})
			if entry.CreatedAt.Unix() > spammer.LastMessage.Unix() {
				spammer.LastMessage = entry.CreatedAt
			}
			spammers[entry.Author.Username] = spammer
		} else {
			spammers[entry.Author.Username] = Spammer{
				ID:          entry.Author.ID,
				Username:    entry.Author.Username,
				Name:        entry.Author.Name,
				URL:         entry.Author.URL,
				LastMessage: entry.CreatedAt,
				Messages: []Message{
					{
						Title:     entry.Title,
						Text:      entry.Text,
						URL:       entry.URL,
						CreatedAt: entry.CreatedAt,
					},
				},
			}
		}
	}

	// rate spammers
	log.Print("Rating potential spammers...")
	if b.openAiKey != "" {
		// AI based detection with OpenAI
		for uid, spammer := range spammers {
			log.Print("Getting AI rating for", spammer.Username)
			sort.Slice(spammer.Messages, func(i, j int) bool {
				return spammer.Messages[i].CreatedAt.Unix() > spammer.Messages[j].CreatedAt.Unix()
			})

			msg := spammer.Messages[0]
			answer, err := openai.GetAiRating(msg.Title+"\n\n"+msg.Text, b.openAiKey)
			if err != nil {
				spammer.SpamRating = "Error: AI rating failed: " + err.Error()
			} else {
				if len(answer.Choices) > 0 {
					spammer.SpamRating = answer.Choices[0].Message.Content
				}
			}
			spammers[uid] = spammer
		}
	} else {
		// our own (stupid, but super accurate) algorithm ;)
		for uid, spammer := range spammers {
			probability := 0.0
			for _, keyword := range b.settings.SpamKeywords {
				for _, message := range spammer.Messages {
					if strings.Contains(strings.ToLower(message.Text), strings.ToLower(keyword)) {
						probability += b.settings.RatingNum
					}
				}
			}
			probInt := math.Round(probability)
			if probInt > 10 {
				probInt = 10
			}
			spammer.SpamRating = strconv.Itoa(int(probInt))
			spammers[uid] = spammer
		}
	}

	// sort and return spammers
	for _, spammer := range spammers {
		result = append(result, spammer)
	}
	sort.Slice(result, func(i, j int) bool {
		return len(result[i].Messages) > len(result[j].Messages)
	})

	return result, nil
}
